#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "rokubimini_ethercat_msgs/Reading.h"
#include "geometry_msgs/WrenchStamped.h"
#include <math.h>
#include <iostream>
#include <sstream>
#include <vector>

//Parameter
static const int scale = 8;
double forcethreshold = 0.8;
double torquethreshold = 0.02;
int delay = 5;

//Global variables
static const int length = 3*pow(2,scale-1);
double a[scale][length];
double d[scale][length];
std::vector<geometry_msgs::WrenchStamped> win;
ros::Publisher filter;


//Filterfunction
double dwt(double threshold)
{
    //Transformation from scale 1 to the desired scale
    for (int j = 1; j < scale; j++)
    {
        for(unsigned int i = 0; i < int(3*pow(2,scale-(j+1))); i++)
        {
            a[j][i] = (a[j-1][2*i] + a[j-1][2*i+1])/sqrt(2);
            if(i == 0)
            {
                d[j][i] = sqrt(2)*((-5.0/16.0)*a[j-1][2*i] + (11.0/16.0)*a[j-1][2*i+1] - (1.0/4.0)*a[j-1][2*i+2] - (1.0/4.0)*a[j-1][2*i+3] + (1.0/16.0)*a[j-1][2*i+4] + (1.0/16.0)*a[j-1][2*i+5]);
            }
            else if(i == int(3*pow(2,scale-(j+1))) - 1)
            {
                d[j][i] = sqrt(2)*((5.0/16.0)*a[j-1][2*i+1] - (11.0/16.0)*a[j-1][2*i] + (1.0/4.0)*a[j-1][2*i-1] + (1.0/4.0)*a[j-1][2*i-2] - (1.0/16.0)*a[j-1][2*i-3] - (1.0/16.0)*a[j-1][2*i-4]);
            }
            else
            {
                d[j][i] = sqrt(2)*((1.0/16.0)*a[j-1][2*i-2] + (1.0/16.0)*a[j-1][2*i-1] - (1.0/2.0)*a[j-1][2*i] + (1.0/2.0)*a[j-1][2*i+1] - (1.0/16.0)*a[j-1][2*i+2] - (1.0/16.0)*a[j-1][2*i+3]);
            }
            //Thresholding
            if(d[j][i] < threshold && d[j][i] > (-threshold))
            {
                d[j][i] = 0;
            }
        }
    }

    //Reconstruction of the signal
    for (int j = scale-1; j > 0; j--)
    {
        for(unsigned int i = 0; i < 3*pow(2,scale-(j+1)); i++)
        {
            if(i == 0)
            {
                a[j-1][2*i+1] = (d[j][i] + (5.0/8.0)*a[j][i] + (1.0/2.0)*a[j][i+1] - (1.0/8.0)*a[j][i+2])/sqrt(2);
                a[j-1][2*i] = (-d[j][i] + (11.0/8.0)*a[j][i] - (1.0/2.0)*a[j][i+1] + (1.0/8.0)*a[j][i+2])/sqrt(2);
            }
            else if(i == (3*pow(2,scale-(j+1)) - 1))
            {
                a[j-1][2*i+1] = (d[j][i] + (1.0/8.0)*a[j][i-2] - (1.0/2.0)*a[j][i-1] + (11.0/8.0)*a[j][i])/sqrt(2);
                a[j-1][2*i] = (-d[j][i] - (1.0/8.0)*a[j][i-2] + (1.0/2.0)*a[j][i-1] + (5.0/8.0)*a[j][i])/sqrt(2);
            }
            else
            {
                a[j-1][2*i+1] = (d[j][i] - (1.0/8.0)*a[j][i-1] + a[j][i] + (1.0/8.0)*a[j][i+1])/sqrt(2);
                a[j-1][2*i] = (-d[j][i] + (1.0/8.0)*a[j][i-1] + a[j][i] - (1.0/8.0)*a[j][i+1])/sqrt(2);
            }
        }
    }

    //returning the value dependent on the delay
    if(delay%2 == 0)
    {
        int i = 3*pow(2,scale-1) - ((delay/2)+1);
        if(delay == 0)
        {
            return (d[0][i] + (1.0/8.0)*a[0][i-2] - (1.0/2.0)*a[0][i-1] + (11.0/8.0)*a[0][i])/sqrt(2);
        }
        else
        {
            return (d[0][i] - (1.0/8.0)*a[0][i-1] + a[0][i] + (1.0/8.0)*a[0][i+1])/sqrt(2);
        }  
    }
    else
    {
        int i = 3*pow(2,scale-1) - (((delay-1)/2)+1);
        if(delay == 1)
        {
            return (-d[0][i] - (1.0/8.0)*a[0][i-2] + (1.0/2.0)*a[0][i-1] + (5.0/8.0)*a[0][i])/sqrt(2);
        }
        else
        {
            return (-d[0][i] + (1.0/8.0)*a[0][i-1] + a[0][i] - (1.0/8.0)*a[0][i+1])/sqrt(2);            
        }
    }
    
}


void callback(const geometry_msgs::WrenchStamped::ConstPtr& msg)
{
    geometry_msgs::WrenchStamped f;
    //While filling up the window, the values get returned without filtering
    if (win.size() < int(3*pow(2,scale))-1)
    {
        win.push_back(*msg);
        if (win.size() >= delay + 1)
        {
            filter.publish(win[win.size()-(delay+1)]);
        }        
    }
    else
    {
        if (win.size() == int(3*pow(2,scale))-1)
        {
            win.push_back(*msg);
        }
        else
        {
            win.erase(win.begin());
            win.push_back(*msg);
        }

        //Force x
        //Transformation from Scale 0 to 1
        for(unsigned int i = 0; i < int(3*pow(2,scale-1)); i++)
        {
            a[0][i] = (win[2*i].wrench.force.x + win[2*i+1].wrench.force.x)/sqrt(2);
            if(i == 0)
            {
                d[0][i] = sqrt(2)*((-5.0/16.0)*win[2*i].wrench.force.x + (11.0/16.0)*win[2*i+1].wrench.force.x - (1.0/4.0)*win[2*i+2].wrench.force.x - (1.0/4.0)*win[2*i+3].wrench.force.x + (1.0/16.0)*win[2*i+4].wrench.force.x + (1.0/16.0)*win[2*i+5].wrench.force.x);
            }
            else if(i == int(3*pow(2,scale-1)) - 1)
            {
                d[0][i] = sqrt(2)*((5.0/16.0)*win[2*i+1].wrench.force.x - (11.0/16.0)*win[2*i].wrench.force.x + (1.0/4.0)*win[2*i-1].wrench.force.x + (1.0/4.0)*win[2*i-2].wrench.force.x - (1.0/16.0)*win[2*i-3].wrench.force.x - (1.0/16.0)*win[2*i-4].wrench.force.x);
            }
            else
            {
                d[0][i] = sqrt(2)*((1.0/16.0)*win[2*i-2].wrench.force.x + (1.0/16.0)*win[2*i-1].wrench.force.x - (1.0/2.0)*win[2*i].wrench.force.x + (1.0/2.0)*win[2*i+1].wrench.force.x - (1.0/16.0)*win[2*i+2].wrench.force.x - (1.0/16.0)*win[2*i+3].wrench.force.x);
            }
            //Thresholding
            if(d[0][i] < forcethreshold && d[0][i] > (-forcethreshold))
            {
                d[0][i] = 0;
            }
        }
        f.wrench.force.x = dwt(forcethreshold);

        //Force y
        //Transformation from Scale 0 to 1
        for(unsigned int i = 0; i < int(3*pow(2,scale-1)); i++)
        {
            a[0][i] = (win[2*i].wrench.force.y + win[2*i+1].wrench.force.y)/sqrt(2);
            if(i == 0)
            {
                d[0][i] = sqrt(2)*((-5.0/16.0)*win[2*i].wrench.force.y + (11.0/16.0)*win[2*i+1].wrench.force.y - (1.0/4.0)*win[2*i+2].wrench.force.y - (1.0/4.0)*win[2*i+3].wrench.force.y + (1.0/16.0)*win[2*i+4].wrench.force.y + (1.0/16.0)*win[2*i+5].wrench.force.y);
            }
            else if(i == int(3*pow(2,scale-1)) - 1)
            {
                d[0][i] = sqrt(2)*((5.0/16.0)*win[2*i+1].wrench.force.y - (11.0/16.0)*win[2*i].wrench.force.y + (1.0/4.0)*win[2*i-1].wrench.force.y + (1.0/4.0)*win[2*i-2].wrench.force.y - (1.0/16.0)*win[2*i-3].wrench.force.y - (1.0/16.0)*win[2*i-4].wrench.force.y);
            }
            else
            {
                d[0][i] = sqrt(2)*((1.0/16.0)*win[2*i-2].wrench.force.y + (1.0/16.0)*win[2*i-1].wrench.force.y - (1.0/2.0)*win[2*i].wrench.force.y + (1.0/2.0)*win[2*i+1].wrench.force.y - (1.0/16.0)*win[2*i+2].wrench.force.y - (1.0/16.0)*win[2*i+3].wrench.force.y);
            }
            //Thresholding
            if(d[0][i] < forcethreshold && d[0][i] > (-forcethreshold))
            {
                d[0][i] = 0;
            }
        }
        f.wrench.force.y = dwt(forcethreshold);


        //Force z
        //Transformation from Scale 0 to 1
        for(unsigned int i = 0; i < int(3*pow(2,scale-1)); i++)
        {
            a[0][i] = (win[2*i].wrench.force.z + win[2*i+1].wrench.force.z)/sqrt(2);
            if(i == 0)
            {
                d[0][i] = sqrt(2)*((-5.0/16.0)*win[2*i].wrench.force.z + (11.0/16.0)*win[2*i+1].wrench.force.z - (1.0/4.0)*win[2*i+2].wrench.force.z - (1.0/4.0)*win[2*i+3].wrench.force.z + (1.0/16.0)*win[2*i+4].wrench.force.z + (1.0/16.0)*win[2*i+5].wrench.force.z);
            }
            else if(i == int(3*pow(2,scale-1)) - 1)
            {
                d[0][i] = sqrt(2)*((5.0/16.0)*win[2*i+1].wrench.force.z - (11.0/16.0)*win[2*i].wrench.force.z + (1.0/4.0)*win[2*i-1].wrench.force.z + (1.0/4.0)*win[2*i-2].wrench.force.z - (1.0/16.0)*win[2*i-3].wrench.force.z - (1.0/16.0)*win[2*i-4].wrench.force.z);
            }
            else
            {
                d[0][i] = sqrt(2)*((1.0/16.0)*win[2*i-2].wrench.force.z + (1.0/16.0)*win[2*i-1].wrench.force.z - (1.0/2.0)*win[2*i].wrench.force.z + (1.0/2.0)*win[2*i+1].wrench.force.z - (1.0/16.0)*win[2*i+2].wrench.force.z - (1.0/16.0)*win[2*i+3].wrench.force.z);
            }
            //Thresholding
            if(d[0][i] < forcethreshold && d[0][i] > (-forcethreshold))
            {
                d[0][i] = 0;
            }
        }
        f.wrench.force.z = dwt(forcethreshold);

        //Torque x
        //Transformation from Scale 0 to 1
        for(unsigned int i = 0; i < int(3*pow(2,scale-1)); i++)
        {
            a[0][i] = (win[2*i].wrench.torque.x + win[2*i+1].wrench.torque.x)/sqrt(2);
            if(i == 0)
            {
                d[0][i] = sqrt(2)*((-5.0/16.0)*win[2*i].wrench.torque.x + (11.0/16.0)*win[2*i+1].wrench.torque.x - (1.0/4.0)*win[2*i+2].wrench.torque.x - (1.0/4.0)*win[2*i+3].wrench.torque.x + (1.0/16.0)*win[2*i+4].wrench.torque.x + (1.0/16.0)*win[2*i+5].wrench.torque.x);
            }
            else if(i == int(3*pow(2,scale-1)) - 1)
            {
                d[0][i] = sqrt(2)*((5.0/16.0)*win[2*i+1].wrench.torque.x - (11.0/16.0)*win[2*i].wrench.torque.x + (1.0/4.0)*win[2*i-1].wrench.torque.x + (1.0/4.0)*win[2*i-2].wrench.torque.x - (1.0/16.0)*win[2*i-3].wrench.torque.x - (1.0/16.0)*win[2*i-4].wrench.torque.x);
            }
            else
            {
                d[0][i] = sqrt(2)*((1.0/16.0)*win[2*i-2].wrench.torque.x + (1.0/16.0)*win[2*i-1].wrench.torque.x - (1.0/2.0)*win[2*i].wrench.torque.x + (1.0/2.0)*win[2*i+1].wrench.torque.x - (1.0/16.0)*win[2*i+2].wrench.torque.x - (1.0/16.0)*win[2*i+3].wrench.torque.x);
            }
            //Thresholding
            if(d[0][i] < torquethreshold && d[0][i] > (-torquethreshold))
            {
                d[0][i] = 0;
            }
        }
        f.wrench.torque.x = dwt(torquethreshold);

        //Torque y
        //Transformation from Scale 0 to 1
        for(unsigned int i = 0; i < int(3*pow(2,scale-1)); i++)
        {
            a[0][i] = (win[2*i].wrench.torque.y + win[2*i+1].wrench.torque.y)/sqrt(2);
            if(i == 0)
            {
                d[0][i] = sqrt(2)*((-5.0/16.0)*win[2*i].wrench.torque.y + (11.0/16.0)*win[2*i+1].wrench.torque.y - (1.0/4.0)*win[2*i+2].wrench.torque.y - (1.0/4.0)*win[2*i+3].wrench.torque.y + (1.0/16.0)*win[2*i+4].wrench.torque.y + (1.0/16.0)*win[2*i+5].wrench.torque.y);
            }
            else if(i == int(3*pow(2,scale-1)) - 1)
            {
                d[0][i] = sqrt(2)*((5.0/16.0)*win[2*i+1].wrench.torque.y - (11.0/16.0)*win[2*i].wrench.torque.y + (1.0/4.0)*win[2*i-1].wrench.torque.y + (1.0/4.0)*win[2*i-2].wrench.torque.y - (1.0/16.0)*win[2*i-3].wrench.torque.y - (1.0/16.0)*win[2*i-4].wrench.torque.y);
            }
            else
            {
                d[0][i] = sqrt(2)*((1.0/16.0)*win[2*i-2].wrench.torque.y + (1.0/16.0)*win[2*i-1].wrench.torque.y - (1.0/2.0)*win[2*i].wrench.torque.y + (1.0/2.0)*win[2*i+1].wrench.torque.y - (1.0/16.0)*win[2*i+2].wrench.torque.y - (1.0/16.0)*win[2*i+3].wrench.torque.y);
            }
            //Thresholding
            if(d[0][i] < torquethreshold && d[0][i] > (-torquethreshold))
            {
                d[0][i] = 0;
            }
        }
        f.wrench.torque.y = dwt(torquethreshold);

        //Torque z
        //Transformation from Scale 0 to 1
        for(unsigned int i = 0; i < int(3*pow(2,scale-1)); i++)
        {
            a[0][i] = (win[2*i].wrench.torque.z + win[2*i+1].wrench.torque.z)/sqrt(2);
            if(i == 0)
            {
                d[0][i] = sqrt(2)*((-5.0/16.0)*win[2*i].wrench.torque.z + (11.0/16.0)*win[2*i+1].wrench.torque.z - (1.0/4.0)*win[2*i+2].wrench.torque.z - (1.0/4.0)*win[2*i+3].wrench.torque.z + (1.0/16.0)*win[2*i+4].wrench.torque.z + (1.0/16.0)*win[2*i+5].wrench.torque.z);
            }
            else if(i == int(3*pow(2,scale-1)) - 1)
            {
                d[0][i] = sqrt(2)*((5.0/16.0)*win[2*i+1].wrench.torque.z - (11.0/16.0)*win[2*i].wrench.torque.z + (1.0/4.0)*win[2*i-1].wrench.torque.z + (1.0/4.0)*win[2*i-2].wrench.torque.z - (1.0/16.0)*win[2*i-3].wrench.torque.z - (1.0/16.0)*win[2*i-4].wrench.torque.z);
            }
            else
            {
                d[0][i] = sqrt(2)*((1.0/16.0)*win[2*i-2].wrench.torque.z + (1.0/16.0)*win[2*i-1].wrench.torque.z - (1.0/2.0)*win[2*i].wrench.torque.z + (1.0/2.0)*win[2*i+1].wrench.torque.z - (1.0/16.0)*win[2*i+2].wrench.torque.z - (1.0/16.0)*win[2*i+3].wrench.torque.z);
            }
            //Thresholding
            if(d[0][i] < torquethreshold && d[0][i] > (-torquethreshold))
            {
                d[0][i] = 0;
            }
        }
        f.wrench.torque.z = dwt(torquethreshold);
        
        f.header.stamp = win[win.size()-(delay+1)].header.stamp;
        f.header.seq = win[win.size()-(delay+1)].header.seq;
        f.header.frame_id = win[win.size()-(delay+1)].header.frame_id;

        filter.publish(f);
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "roswave");
    ros::NodeHandle n;
    ros::Subscriber subforce = n.subscribe("/rokubimini_cosmo/ft_sensor/wrench", 100000, callback);  
    filter = n.advertise<geometry_msgs::WrenchStamped>("cleanz",1000);
    ros::spin();
}